

#include <SPI.h>
#include <MFRC522.h>
#include <MFRC522Hack.h>
#define RST_PIN         9          // Configurable, see typical pin layout above
#define SS_PIN          10         // Configurable, see typical pin layout above

MFRC522 mfrc522(SS_PIN, RST_PIN);  // Create MFRC522 instance
MFRC522Hack mfrc522Hack(&mfrc522);  // Create MFRC522Hack in
byte nuidPICC[4];


int third = 4;
bool status = false;
bool isReadSuccessfull = false;
void setup() {

	SPI.begin();			// Init SPI bus
	mfrc522.PCD_Init();		// Init MFRC522
	mfrc522.PCD_DumpVersionToSerial();	// Show details of PCD - MFRC522 Card Reader details
	Serial.println(F("Scan PICC to see UID, type, and data blocks..."));

	Serial.begin(9600);


	pinMode(third, INPUT);


	digitalWrite(third, LOW);

}

void printDec(byte *buffer, byte bufferSize) {
	for (byte i = 0; i < bufferSize; i++) {
		Serial.print(buffer[i] < 0x10 ? " 0" : " ");
		Serial.print(buffer[i], DEC);
	}
}
void printHex(byte *buffer, byte bufferSize) {
	for (byte i = 0; i < bufferSize; i++) {
		Serial.print(buffer[i] < 0x10 ? " 0" : " ");
		Serial.print(buffer[i], HEX);
	}
}
bool checkIsClassic()
{

	if (mfrc522.uid.size > 4)
	{
		Serial.println("NOT CLASSIC");
		return false;
	}
	else return true;
}
bool bufferUID()
{

	if (mfrc522.uid.uidByte[0] != nuidPICC[0] ||
		mfrc522.uid.uidByte[1] != nuidPICC[1] ||
		mfrc522.uid.uidByte[2] != nuidPICC[2] ||
		mfrc522.uid.uidByte[3] != nuidPICC[3])
	{
		// Store NUID into nuidPICC array
		for (byte i = 0; i < 4; i++)
		{
			nuidPICC[i] = mfrc522.uid.uidByte[i];
		}
		return true;
	}
	else
	{

		return false;
	}
}
bool attemptReadCard()
{
	if (mfrc522.PICC_IsNewCardPresent() && mfrc522.PICC_ReadCardSerial()) return true;
	else return false;
}
void loop() {



	status = digitalRead(third);

	if (status)
	{




		if (attemptReadCard())
		{


			
			if (!checkIsClassic()) return;
			bufferUID();

			Serial.println(F(" A new card has been detected."));




			Serial.print(F("In hex: "));
			printHex(mfrc522.uid.uidByte, mfrc522.uid.size);
			
			Serial.println("");
		
			mfrc522.PICC_HaltA();
			return;





		}


	}
	else
	{
		
		if (attemptReadCard())
		{

			if (!checkIsClassic()) return;

			

			if (mfrc522Hack.MIFARE_SetUid(nuidPICC, 4, true))
			{
				Serial.println("SUCCESSFULY REWRITED UID");
				
				
			}
			else Serial.println("CANT REWRITE UID");
			delay(2000);
			return;

		}

	}


}